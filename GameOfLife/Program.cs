﻿using System;
using System.Threading;

class Program
{
    static void Main()
    {
        // Generation #0
        Console.WriteLine("Generation #0");
        int[] generation0 = ConvertStringToIntArray("000111010");
        Print(generation0);
        
        // Generation #1
        Console.WriteLine("Generation #1");
        int[] generation1 = GetNextGeneration(generation0);
        Print(generation1);

        // Generation #2
        Console.WriteLine("Generation #2");
        int[] generation2 = GetNextGeneration(generation1);
        Print(generation2);

        // Generation #3
        Console.WriteLine("Generation #3");
        int[] generation3 = GetNextGeneration(generation2);
        Print(generation3);

        // Generation #4
        Console.WriteLine("Generation #4");
        int[] generation4 = GetNextGeneration(generation3);
        Print(generation4);

        // Generation #5
        Console.WriteLine("Generation #5");
        string generation5 = ConvertIntArrayToString(GetNextGeneration(generation4));
        Print(generation5);
    }

    static int[] ConvertStringToIntArray(string str)
    {
        int[] array = new int[str.Length];

        for (int i = 0; i < str.Length; i++)
        {
            array[i] = (int)char.GetNumericValue(str[i]);
        }

        return array;
    }
    static string ConvertIntArrayToString(int[] array)
    {
        return string.Join("", array);
    }

    static int[] ConvertMatrixToArray(int[,] matrix)
    {
        int rows = matrix.GetUpperBound(0) + 1; // adding 1 is necessary because GetUpperBound returns index, not length
        int columns = matrix.GetUpperBound(1) + 1; // adding 1 is necessary because GetUpperBound returns index, not length
        int[] array = new int[rows * columns];
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                array[row * columns + column] = matrix[row, column];
            }
        }
        return array;
    }
    static int[,] ConvertArrayToMatrix(int[] array)
    {
        int sideLength = Convert.ToInt32(Math.Sqrt(array.Length));
        int rows = sideLength;
        int columns = sideLength;
        int[,] matrix = new int[rows, columns];
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                matrix[row, column] = array[row * columns + column];
            }
        }
        return matrix;
    }

    static int[,] GetNextGeneration(int[,] previousGeneration)
    {
        int totalRows = previousGeneration.GetUpperBound(0) + 1;
        int totalColumns = previousGeneration.GetUpperBound(1) + 1;
        var nextGeneration = new int[totalRows, totalColumns];
        for (int row = 0; row < totalRows; row++)
        {
            for (int column = 0; column < totalColumns; column++)
            {
                nextGeneration[row, column] = ApplyGameRules(row, column, previousGeneration);
            }
        }
        return nextGeneration;
    }
    static int[] GetNextGeneration(int[] previousGeneration)
    {
        return ConvertMatrixToArray(GetNextGeneration(ConvertArrayToMatrix(previousGeneration)));
    }

    static void Print(int[] input)
    {
        Print(string.Join("", input));
    }
    static void Print(string input)
    {
        int sideLength = Convert.ToInt32(Math.Sqrt(input.Length));
        int rows = sideLength;
        int columns = sideLength;
        for (int row = 0; row < rows; row++)
        {
            string line = "";
            for (int column = 0; column < columns; column++)
            {
                line += $"{input[row * columns + column]} ";
            }
            Console.WriteLine(line.Trim());
        }

        Thread.Sleep(1000);
        Console.Clear();
    }

    static int ApplyGameRules(int cellRow, int cellColumn, int[,] matrix)
    {
        int aliveNeighborsCount = GetAliveNeighborsCount(cellRow, cellColumn, matrix);

        // 0 - dead
        // 1 - alive
        int cellStateInNextGeneration;

        if (aliveNeighborsCount == 3)
        {
            cellStateInNextGeneration = 1;
        }
        else
        {
            cellStateInNextGeneration = matrix[cellRow, cellColumn] == 1 && aliveNeighborsCount == 2 ? 1 : 0;
        }

        return cellStateInNextGeneration;
    }
    static int GetAliveNeighborsCount(int currentCellRow, int currentCellColumn, int[,] matrix)
    {
        int maxRows = matrix.GetUpperBound(0);
        int aliveNeighboursCount = 0;

        // top
        bool topCellExists = currentCellRow - 1 >= 0;
        bool isTopCellAlive = topCellExists && matrix[currentCellRow - 1, currentCellColumn] == 1;
        aliveNeighboursCount += isTopCellAlive ? 1 : 0;

        // bottom
        bool bottomCellExists = currentCellRow + 1 <= maxRows;
        bool isBottomCellAlive = bottomCellExists && matrix[currentCellRow + 1, currentCellColumn] == 1;
        aliveNeighboursCount += isBottomCellAlive ? 1 : 0;

        // left
        bool leftCellExists = currentCellColumn - 1 >= 0;
        bool isLeftCellAlive = leftCellExists && matrix[currentCellRow, currentCellColumn - 1] == 1;
        aliveNeighboursCount += isLeftCellAlive ? 1 : 0;

        // right
        bool rightCellExists = currentCellColumn + 1 <= matrix.GetUpperBound(1);
        bool isRightCellAlive = rightCellExists && matrix[currentCellRow, currentCellColumn + 1] == 1;
        aliveNeighboursCount += isRightCellAlive ? 1 : 0;

        // top-left
        var isTopLeftCellAlive = topCellExists && leftCellExists && matrix[currentCellRow - 1, currentCellColumn - 1] == 1;
        aliveNeighboursCount += isTopLeftCellAlive ? 1 : 0;

        // top-right
        var isTopRightCellAlive = topCellExists && rightCellExists && matrix[currentCellRow - 1, currentCellColumn + 1] == 1;
        aliveNeighboursCount += isTopRightCellAlive ? 1 : 0;

        // bottom-left
        var isBottomLeftCellAlive = bottomCellExists && leftCellExists && matrix[currentCellRow + 1, currentCellColumn - 1] == 1;
        aliveNeighboursCount += isBottomLeftCellAlive ? 1 : 0;

        // bottom-right
        var isBottomRightCellAlive = bottomCellExists && rightCellExists && matrix[currentCellRow + 1, currentCellColumn + 1] == 1;
        aliveNeighboursCount += isBottomRightCellAlive ? 1 : 0;

        // result
        return aliveNeighboursCount;
    }
}

